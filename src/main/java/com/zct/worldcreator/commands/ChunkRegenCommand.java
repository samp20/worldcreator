/*
 * Copyright 2012 zombiechickentaco.com
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.zct.worldcreator.commands;

import com.zct.utils.Validate;
import com.zct.utils.commands.Command;
import com.zct.worldcreator.WorldCreator;
import org.bukkit.Chunk;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class ChunkRegenCommand extends Command {

    public ChunkRegenCommand(String name) {
        super(name);
    }

    @Override
    public boolean onCommand(CommandSender sender, String command, List<String> args) {
        Player player = Validate.asPlayer(sender);
        Validate.isTrue(player.hasPermission("zct.worldcreator.regen.chunk"),"You do not have permission for this command");

        Chunk c = player.getWorld().getChunkAt(player.getLocation());
        final int cx = c.getX();
        final int cz = c.getZ();

        final int rx = cx<0? (cx-31)/32 : cx/32;
        final int rz = cz<0? (cz-31)/32 : cz/32;
        
        WorldCreator.getInstance().getTextureCache().remove(player.getWorld(), rx, rz);
        player.getWorld().regenerateChunk(c.getX(),c.getZ());
        player.sendMessage("Chunk " + cx + ":" + cz + " Regenerated");
        return true;
    }
}
