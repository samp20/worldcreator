/*
 * Copyright 2012 zombiechickentaco.com
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.zct.worldcreator.commands;

import com.zct.utils.Validate;
import com.zct.utils.commands.Command;
import com.zct.worldcreator.WorldCreator;
import com.zct.worldcreator.model.WorldConfig;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class ConfigDefaultCommand extends Command {

    public ConfigDefaultCommand(String name) {
        super(name);
    }

    @Override
    public boolean onCommand(CommandSender sender, String command, List<String> args) {
        Validate.isTrue(sender.hasPermission("zct.worldcreator.config.default"), "You do not have permission for this command");
        World world;
        String defaultVal = getArg(args).toLowerCase();

        String worldName;
        if((worldName = tryGetArg(args)) != null) {
            world = Bukkit.getWorld(worldName);
            if(world == null) throw new IllegalArgumentException("World " + worldName + " doesn't exist");
        }else {
            final Player player = Validate.asPlayer(sender);
            world = player.getWorld();
        }
        WorldConfig config = WorldCreator.getInstance().getWorldCache().get(world);

        if(defaultVal.equals("biomes")) {
            config.loadDefaultBiomes();
            sender.sendMessage("Default biomes have been loaded. Use '/wcr config save' to save to file.");
        }else if(defaultVal.equals("blocks")) {
            config.loadDefaultBlockPatterns();
            sender.sendMessage("Default blocks have been loaded. Use '/wcr config save' to save to file.");
        }else if(defaultVal.equals("objects")) {
            config.loadDefaultWorldObjects();
            sender.sendMessage("Default objects have been loaded. Use '/wcr config save' to save to file.");
        }else if(defaultVal.equals("liquid")) {
            config.setDefaultLiquid();
            sender.sendMessage("Default liquid has been set. Use '/wcr config save' to save to file.");
        }else if(defaultVal.equals("biomeice")) {
            config.setDefaultBiomeIce();
            sender.sendMessage("Biome ice set to enabled default. Use '/wcr config save' to save to file.");
        }else if(defaultVal.equals("all")) {
            config.loadDefaultBiomes();
            config.loadDefaultBlockPatterns();
            config.loadDefaultWorldObjects();
            config.setDefaultLiquid();
            config.setDefaultBiomeIce();
            sender.sendMessage("Entire config set to defaults. Use '/wcr config save' to save to file.");
        }else throw new IllegalArgumentException("Unknown config option " + defaultVal);

        return true;
    }
}
