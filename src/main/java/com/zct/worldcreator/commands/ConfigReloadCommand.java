/*
 * Copyright 2012 zombiechickentaco.com
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.zct.worldcreator.commands;


import com.zct.utils.Validate;
import com.zct.utils.commands.Command;
import com.zct.worldcreator.WorldCreator;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class ConfigReloadCommand extends Command {
    public ConfigReloadCommand(String name) {
        super(name);
    }

    @Override
    public boolean onCommand(CommandSender sender, String command, List<String> args) {
        Validate.isTrue(sender.hasPermission("zct.worldcreator.config.reload"),"You do not have permission for this command");
        World world;
        String arg;
        if((arg = tryGetArg(args)) != null) {
            world = Bukkit.getWorld(arg);
            if(world == null) throw new IllegalArgumentException("World " + arg + " doesn't exist");
        }else {
            final Player player = Validate.asPlayer(sender);
            world = player.getWorld();
        }
        WorldCreator.getInstance().getWorldCache().remove(world);

        sender.sendMessage("Config reloaded for world " + world.getName());
        return true;
    }
}
