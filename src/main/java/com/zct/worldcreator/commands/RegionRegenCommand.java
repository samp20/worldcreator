/*
 * Copyright 2012 zombiechickentaco.com
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.zct.worldcreator.commands;

import com.zct.utils.Validate;
import com.zct.utils.commands.Command;
import com.zct.utils.threading.IterativeTask;
import com.zct.worldcreator.WorldCreator;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class RegionRegenCommand extends Command {

    public RegionRegenCommand(String name) {
        super(name);
    }

    @Override
    public boolean onCommand(CommandSender sender, String command, List<String> args) {
        final Player player = Validate.asPlayer(sender);
        Validate.isTrue(player.hasPermission("zct.worldcreator.regen.region"),"You do not have permission for this command");
        final World world = player.getWorld();
        Chunk c = world.getChunkAt(player.getLocation());
        int cx = c.getX();
        int cz = c.getZ();

        final int rx = cx<0? (cx-31)/32 : cx/32;
        final int rz = cz<0? (cz-31)/32 : cz/32;

        WorldCreator.getInstance().getTextureCache().remove(world, rx, rz);

        WorldCreator.getInstance().getTaskManager().add(new IterativeTask() {
            int minX = rx*32;
            int minZ = rz*32;
            int maxX = minX+32;
            int maxZ = minZ+32;

            int curX = minX;
            int curZ = minZ;

            @Override
            public boolean doNext() {
                if(!world.isChunkLoaded(curX,curZ)) world.loadChunk(curX,curZ);
                world.regenerateChunk(curX,curZ);
                world.unloadChunkRequest(curX,curZ);
                curX++;
                if(curX == maxX) {
                    curX = minX;
                    curZ++;
                }
                if(curZ == maxZ) {
                    player.sendMessage("Region " + rx + ":" + rz + " regenerated successfully. Log in and out to see results");
                    return false;
                }
                return true;
            }
        });

        player.sendMessage("Regenerating region (" + rx + ":" + rz + "). This may take a while so please be patient.");
        return true;
    }
}
