/*
 * Copyright 2012 zombiechickentaco.com
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.zct.worldcreator.cache;

import com.zct.utils.cache.LRUCache;
import org.bukkit.World;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class TextureCache {
    public enum TextureType {
        HEIGHT_MAP,
        BLOCK_MAP,
        BIOME_MAP,
        OBJECT_MAP
    }
    
    private class TextureID {
        public World world;
        public int x;
        public int z;
        public TextureType type;
        
        public TextureID(World world, int x, int z, TextureType type) {
            this.world = world;
            this.x = x;
            this.z = z;
            this.type = type;
        }
        
        public File toFile() {
            String typeName = "";
            switch (type) {
                case HEIGHT_MAP:
                    typeName = "heightmap";
                    break;
                case BLOCK_MAP:
                    typeName = "blocks";
                    break;
                case BIOME_MAP:
                    typeName = "biomes";
                    break;
                case OBJECT_MAP:
                    typeName = "objects";
                    break;
            }
            
            return new File(world.getWorldFolder(), "data" + File.separator + typeName + "_" + x + "_" + z + ".png");
        }

        @Override
        public int hashCode() {
            return world.hashCode() ^ ((Integer)x).hashCode() ^ ((Integer)z).hashCode() ^ (type.hashCode()*17);
        }

        @Override
        public boolean equals(Object obj) {
            if(!(obj instanceof TextureID)) return false;
            TextureID other = (TextureID)obj;
            return other.type.equals(type) && other.world.equals(world) && other.x == x && other.z == z;
        }
    }
    
    private LRUCache<TextureID, BufferedImage> textures;
    
    public TextureCache(int maxTextures) {
        textures = new LRUCache<TextureID, BufferedImage>(maxTextures);
    }
    
    public BufferedImage get(World world, int x, int z, TextureType type) {
        TextureID id = new TextureID(world, x, z, type);
        BufferedImage image = null;
        if(textures.containsKey(id)) {
            image = textures.get(id);
        }else{
            File file = id.toFile();
            try {
                image = ImageIO.read(file);
            } catch (IOException ignored) {
            }
            textures.put(id,image); //Even if the image is null we will still cache it to save file IO. To load a new image the cache will need to be cleared
        }

        return image;
    }

    public void remove(World world, int x, int z) {
        remove(world, x, z, TextureType.BIOME_MAP);
        remove(world, x, z, TextureType.BLOCK_MAP);
        remove(world, x, z, TextureType.HEIGHT_MAP);
        remove(world, x, z, TextureType.OBJECT_MAP);
    }
    
    public void remove(World world, int x, int z, TextureType type) {
        TextureID id = new TextureID(world, x, z, type);
        textures.remove(id);
    }

    public void clear() {
        textures.clear();
    }
}
