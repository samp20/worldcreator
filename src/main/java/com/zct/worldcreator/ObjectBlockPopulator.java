/*
 * Copyright 2012 zombiechickentaco.com
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.zct.worldcreator;

import com.zct.worldcreator.cache.TextureCache;
import com.zct.worldcreator.model.WorldConfig;
import com.zct.worldcreator.model.WorldObject;
import org.bukkit.*;
import org.bukkit.generator.BlockPopulator;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

public class ObjectBlockPopulator extends BlockPopulator {
    
    @Override
    public void populate(World world, Random random, Chunk chunk) {
        int cx = chunk.getX();
        int cz = chunk.getZ();
        
        int rx = cx<0? (cx-31)/32 : cx/32;
        int rz = cz<0? (cz-31)/32 : cz/32;

        WorldConfig config = WorldCreator.getInstance().getWorldCache().get(world);
        TextureCache cache = WorldCreator.getInstance().getTextureCache();

        BufferedImage objects = getSubImage(cache.get(world, rx, rz, TextureCache.TextureType.OBJECT_MAP), cx, cz);

        BufferedImage heightMap = getSubImage(cache.get(world, rx, rz, TextureCache.TextureType.HEIGHT_MAP), cx, cz);

        if(objects == null || heightMap == null) return;
        
        for(int x=0;x<16;x++) {
            for(int z=0;z<16;z++) {
                int rgb = objects.getRGB(x,z);

                Color c = new Color(heightMap.getRGB(x,z));
                int maxHeight = Math.max(c.getRed(),c.getBlue());

                WorldObject object = config.getWorldObject(rgb);
                if(object == null) continue;
                Location loc = new Location(world, x+cx*16, maxHeight, z+cz*16);
                switch (object) {
                    //Trees
                    case TREE:
                        world.generateTree(loc, TreeType.TREE);
                        break;
                    case BIG_TREE:
                        world.generateTree(loc, TreeType.BIG_TREE);
                        break;
                    case REDWOOD_TREE:
                        world.generateTree(loc, TreeType.REDWOOD);
                        break;
                    case TALL_REDWOOD_TREE:
                        world.generateTree(loc, TreeType.TALL_REDWOOD);
                        break;
                    case BIRCH_TREE:
                        world.generateTree(loc, TreeType.BIRCH);
                        break;
                    case JUNGLE_TREE:
                        world.generateTree(loc, TreeType.JUNGLE);
                        break;
                    case SMALL_JUNGLE_TREE:
                        world.generateTree(loc, TreeType.SMALL_JUNGLE);
                        break;
                    case JUNGLE_BUSH:
                        world.generateTree(loc, TreeType.JUNGLE_BUSH);
                        break;
                    case BIG_RED_MUSHROOM:
                        world.generateTree(loc, TreeType.RED_MUSHROOM);
                        break;
                    case BIG_BROWN_MUSHROOM:
                        world.generateTree(loc, TreeType.BROWN_MUSHROOM);
                        break;
                    case SWAMP_TREE:
                        world.generateTree(loc, TreeType.SWAMP);
                        break;
                    //Misc
                    case RED_ROSE:
                        world.getBlockAt(loc).setType(Material.RED_ROSE);
                        break;
                    case YELLOW_FLOWER:
                        world.getBlockAt(loc).setType(Material.YELLOW_FLOWER);
                        break;
                    case DEAD_BUSH:
                        world.getBlockAt(loc).setType(Material.DEAD_BUSH);
                        break;
                    case TALL_GRASS:
                        world.getBlockAt(loc).setType(Material.LONG_GRASS);
                        world.getBlockAt(loc).setData((byte)1);
                        break;
                    case DEAD_SHRUB:
                        world.getBlockAt(loc).setType(Material.LONG_GRASS);
                        world.getBlockAt(loc).setData((byte)0);
                        break;
                    case FERN:
                        world.getBlockAt(loc).setType(Material.LONG_GRASS);
                        world.getBlockAt(loc).setData((byte)2);
                        break;
                    case RED_MUSHROOM:
                        world.getBlockAt(loc).setType(Material.RED_MUSHROOM);
                        break;
                    case BROWN_MUSHROOM:
                        world.getBlockAt(loc).setType(Material.BROWN_MUSHROOM);
                        break;
                    case CACTUS:
                        world.getBlockAt(loc).setType(Material.CACTUS);
                        world.getBlockAt(loc.add(0.0,1.0,0.0)).setType(Material.CACTUS);
                        world.getBlockAt(loc.add(0.0,1.0,0.0)).setType(Material.CACTUS);
                        break;
                    case SUGAR_CANE:
                        world.getBlockAt(loc).setType(Material.SUGAR_CANE_BLOCK);
                        world.getBlockAt(loc.add(0.0,1.0,0.0)).setType(Material.SUGAR_CANE_BLOCK);
                        world.getBlockAt(loc.add(0.0,1.0,0.0)).setType(Material.SUGAR_CANE_BLOCK);
                        break;
                    //Wools
                    case LIGHT_GRAY_WOOL:
                        world.getBlockAt(loc).setTypeIdAndData(Material.WOOL.getId(), DyeColor.SILVER.getData(), true);
                        break;
                    case GRAY_WOOL:
                        world.getBlockAt(loc).setTypeIdAndData(Material.WOOL.getId(), DyeColor.GRAY.getData(), true);
                        break;
                    case BLACK_WOOL:
                        world.getBlockAt(loc).setTypeIdAndData(Material.WOOL.getId(), DyeColor.BLACK.getData(), true);
                        break;
                    case RED_WOOL:
                        world.getBlockAt(loc).setTypeIdAndData(Material.WOOL.getId(), DyeColor.RED.getData(), true);
                        break;
                    case ORANGE_WOOL:
                        world.getBlockAt(loc).setTypeIdAndData(Material.WOOL.getId(), DyeColor.ORANGE.getData(), true);
                        break;
                    case YELLOW_WOOL:
                        world.getBlockAt(loc).setTypeIdAndData(Material.WOOL.getId(), DyeColor.YELLOW.getData(), true);
                        break;
                    case LIME_WOOL:
                        world.getBlockAt(loc).setTypeIdAndData(Material.WOOL.getId(), DyeColor.LIME.getData(), true);
                        break;
                    case GREEN_WOOL:
                        world.getBlockAt(loc).setTypeIdAndData(Material.WOOL.getId(), DyeColor.GREEN.getData(), true);
                        break;
                    case LIGHT_BLUE_WOOL:
                        world.getBlockAt(loc).setTypeIdAndData(Material.WOOL.getId(), DyeColor.LIGHT_BLUE.getData(), true);
                        break;
                    case CYAN_WOOL:
                        world.getBlockAt(loc).setTypeIdAndData(Material.WOOL.getId(), DyeColor.CYAN.getData(), true);
                        break;
                    case BLUE_WOOL:
                        world.getBlockAt(loc).setTypeIdAndData(Material.WOOL.getId(), DyeColor.BLUE.getData(), true);
                        break;
                    case PURPLE_WOOL:
                        world.getBlockAt(loc).setTypeIdAndData(Material.WOOL.getId(), DyeColor.PURPLE.getData(), true);
                        break;
                    case MAGENTA_WOOL:
                        world.getBlockAt(loc).setTypeIdAndData(Material.WOOL.getId(), DyeColor.MAGENTA.getData(), true);
                        break;
                    case PINK_WOOL:
                        world.getBlockAt(loc).setTypeIdAndData(Material.WOOL.getId(), DyeColor.PINK.getData(), true);
                        break;
                    case BROWN_WOOL:
                        world.getBlockAt(loc).setTypeIdAndData(Material.WOOL.getId(), DyeColor.BROWN.getData(), true);
                        break;
                }
            }
        }
    }

    private BufferedImage getSubImage(BufferedImage source, int cx, int cz) {
        if(source==null) return null;
        return source.getSubimage((cx&0x1f)*16,(cz&0x1f)*16,16,16);
    }
    
}
