/*
 * Copyright 2012 zombiechickentaco.com
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.zct.worldcreator;

import com.zct.utils.commands.CommandManager;
import com.zct.utils.commands.SubCommand;
import com.zct.utils.threading.DistributedTaskManager;
import com.zct.worldcreator.cache.TextureCache;
import com.zct.worldcreator.cache.WorldCache;
import com.zct.worldcreator.commands.*;
import com.zct.worldcreator.model.WorldConfig;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Logger;

public class WorldCreator extends JavaPlugin {
    public static Logger logger;

    private static WorldCreator instance;

    private CommandManager commandManager = null;
    private DistributedTaskManager taskManager = null;
    private TextureCache textureCache = null;
    private WorldCache worldCache = null;

    @Override
    public void onLoad() {
        logger = getLogger();
        instance = this;
    }

    @Override
    public void onDisable() {
        for(WorldConfig config : worldCache.getLoadedWorlds()) {
            config.saveConfig();
        }
    }

    @Override
    public void onEnable() {
        //If ZCTCore is loaded then use it's command manager
        if((commandManager = getServer().getServicesManager().load(CommandManager.class)) == null)
            commandManager = new CommandManager();

        commandManager.addCommands(
                new SubCommand("wcr",
                        new SubCommand("regen",
                                new ChunkRegenCommand("chunk"),
                                new RegionRegenCommand("region")
                        ),
                        new SubCommand("config",
                                new ConfigReloadCommand("reload"),
                                new ConfigSaveCommand("save"),
                                new ConfigDefaultCommand("default")
                        )
                )
        );

        worldCache = new WorldCache();
        textureCache = new TextureCache(10);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        return commandManager.onCommand(sender,command.getName(),args);
    }

    @Override
    public ChunkGenerator getDefaultWorldGenerator(String worldName, String id) {
        return new WorldCreatorChukGenerator();
    }

    public TextureCache getTextureCache() {
        return textureCache;
    }

    public WorldCache getWorldCache() {
        return worldCache;
    }

    public CommandManager getCommandManager() {
        return commandManager;
    }

    public DistributedTaskManager getTaskManager() {
        if(taskManager == null) {
            //If ZCTCore is loaded then use it's TaskManager
            if((taskManager = getServer().getServicesManager().load(DistributedTaskManager.class)) == null)
                taskManager = new DistributedTaskManager(this, DistributedTaskManager.TICK_NANOS/10);
        }
        return taskManager;
    }

    public static WorldCreator getInstance() {
        return instance;
    }
}
