/*
 * Copyright 2012 zombiechickentaco.com
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.zct.worldcreator.model;

import com.google.gson.*;
import com.zct.worldcreator.WorldCreator;
import com.zct.worldcreator.model.serializers.BiomeSerializer;
import com.zct.worldcreator.model.serializers.MaterialSerializer;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Biome;

import java.io.*;
import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;

public class WorldConfig {
    public World world;
    private File configFile;
    public final int version = 3;
    public final Map<Integer, BlockPattern> blockPatterns = new LinkedHashMap<Integer, BlockPattern>();
    public final Map<Integer, WorldObject> worldObjects = new LinkedHashMap<Integer, WorldObject>();
    public final Map<Integer, Biome> biomes = new LinkedHashMap<Integer, Biome>();
    public Material liquid = Material.STATIONARY_WATER;
    public boolean biomeIce = true;
    
    public WorldConfig() {
        this(true);
    }

    public WorldConfig(boolean loadDefaults) {
        if(loadDefaults) {
            loadDefaultBiomes();
            loadDefaultBlockPatterns();
            loadDefaultWorldObjects();
        }
    }
    
    public static WorldConfig createNew(World world) {
        WorldConfig config = null;
        GsonBuilder builder = new GsonBuilder();
        builder.disableHtmlEscaping().setPrettyPrinting();
        loadJsonBuilder(builder);
        File configFile = new File(world.getWorldFolder(), "data" + File.separator + "config.json");
        try {

            BufferedReader reader = new BufferedReader(new FileReader(configFile));
            config = builder.create().fromJson(reader, WorldConfig.class);
            reader.close();
        } catch (FileNotFoundException e) {
            WorldCreator.logger.warning("Unable to find configuration for world " + world.getName());
        } catch (IOException e) {
            WorldCreator.logger.log(Level.WARNING, "Error loading configuration for world " + world.getName(), e);
        }

        if(config == null) {
            config = new WorldConfig();
        }

        if(config.biomes.isEmpty()) config.loadDefaultBiomes();
        if(config.blockPatterns.isEmpty()) config.loadDefaultBlockPatterns();
        if(config.worldObjects.isEmpty()) config.loadDefaultWorldObjects();
        config.configFile = configFile;
        return config;
    }

    public boolean saveConfig() {
        GsonBuilder builder = new GsonBuilder();
        builder.disableHtmlEscaping().setPrettyPrinting();
        loadJsonBuilder(builder);
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(configFile));
            builder.create().toJson(this, WorldConfig.class, writer);
            writer.flush();
            writer.close();
            return true;
        } catch (IOException e) {
            WorldCreator.logger.log(Level.WARNING, "Error saving configuration for world " + world.getName(), e);
        }
        return false;
    }

    public Biome getBiome(int color) {
        color = color & 0xffffff;
        Biome biome = biomes.get(color);
        if(biome == null) biome = biomes.values().iterator().next();
        return biome;
    }

    public Biome getDefaultBiome() {
        return biomes.values().iterator().next();
    }
    
    public BlockPattern getBlockPattern(int color) {
        color = color & 0xffffff;
        BlockPattern pattern = blockPatterns.get(color);
        if(pattern == null) pattern = blockPatterns.values().iterator().next();
        return pattern;
    }

    public WorldObject getWorldObject(int color) {
        color = color & 0xffffff;
        return worldObjects.get(color);
    }

    public void setDefaultLiquid() {
        liquid = Material.STATIONARY_WATER;
    }

    public void setDefaultBiomeIce() {
       biomeIce = true;
    }
    
    public void loadDefaultBlockPatterns() {
        blockPatterns.clear();
        
        blockPatterns.put(0x4c853c,new BlockPattern(1,
                Material.BEDROCK.getId(),
                Material.STONE.getId(),
                Material.DIRT.getId(),
                Material.DIRT.getId(),
                Material.DIRT.getId(),
                Material.DIRT.getId(),
                Material.GRASS.getId()));

        blockPatterns.put(0x209900, new BlockPattern(1,
                Material.BEDROCK.getId(),
                Material.DIRT.getId(),
                Material.GRASS.getId()));

        blockPatterns.put(0xe9ea33, new BlockPattern(1,
                Material.BEDROCK.getId(),
                Material.STONE.getId(),
                Material.SANDSTONE.getId(),
                Material.SANDSTONE.getId(),
                Material.SANDSTONE.getId(),
                Material.SANDSTONE.getId(),
                Material.SAND.getId(),
                Material.SAND.getId(),
                Material.SAND.getId(),
                Material.SAND.getId()));

        blockPatterns.put(0xc2c441, new BlockPattern(1,
                Material.BEDROCK.getId(),
                Material.SANDSTONE.getId(),
                Material.SAND.getId(),
                Material.SAND.getId(),
                Material.SAND.getId(),
                Material.SAND.getId()));

    }

    public void loadDefaultBiomes() {
        biomes.clear();
        biomes.put(0x69a41b,Biome.FOREST);
        //biomes.put(0x13ce25,Biome.RAINFOREST); //Invalid
        //biomes.put(0x72a482,Biome.SWAMPLAND);
        //biomes.put(0x000000,Biome.SEASONAL_FOREST);
        //biomes.put(0xa9cd4a,Biome.SAVANNA);  //Invalid
        //biomes.put(0x000000,Biome.SHRUBLAND);
        biomes.put(0xc9c3b7,Biome.TAIGA);
        biomes.put(0xf8f55f,Biome.DESERT);
        //biomes.put(0x90d49f,Biome.PLAINS);
        //biomes.put(0xb9b840,Biome.ICE_DESERT);
        //biomes.put(0xe8e8e8,Biome.TUNDRA);
        //biomes.put(0x8b0113,Biome.HELL);
        //biomes.put(0xffffff,Biome.SKY);
        biomes.put(0x3f47f2,Biome.OCEAN);
        //biomes.put(0x7379ec,Biome.RIVER);
        //biomes.put(0x928384,Biome.EXTREME_HILLS);
        //biomes.put(0x1aa5cb,Biome.FROZEN_OCEAN);
        //biomes.put(0x47c8eb,Biome.FROZEN_RIVER);
        //biomes.put(0x000000,Biome.ICE_PLAINS);
        //biomes.put(0x000000,Biome.ICE_MOUNTAINS);
        biomes.put(0xb26a9b,Biome.MUSHROOM_ISLAND);
        //biomes.put(0x995ec3,Biome.MUSHROOM_SHORE);
        //biomes.put(0xffe58e,Biome.BEACH);
        //biomes.put(0x000000,Biome.DESERT_HILLS);
        //biomes.put(0x000000,Biome.FOREST_HILLS);
        //biomes.put(0x000000,Biome.TAIGA_HILLS);
        //biomes.put(0x000000,Biome.SMALL_MOUNTAINS);
        biomes.put(0x1ca52a,Biome.JUNGLE);
        //biomes.put(0x68b333,Biome.JUNGLE_HILLS);
    }

    public void loadDefaultWorldObjects() {
        worldObjects.clear();

        worldObjects.put(0x039713, WorldObject.TREE);
        worldObjects.put(0x1d7c28, WorldObject.BIG_TREE);
        worldObjects.put(0x014d00, WorldObject.REDWOOD_TREE);
        worldObjects.put(0x2d4b2d, WorldObject.TALL_REDWOOD_TREE);
        worldObjects.put(0x76bb7d, WorldObject.BIRCH_TREE);
        worldObjects.put(0x00d818, WorldObject.JUNGLE_TREE);
        worldObjects.put(0x1eed35, WorldObject.SMALL_JUNGLE_TREE);
        worldObjects.put(0x3fc84e, WorldObject.JUNGLE_BUSH);
        worldObjects.put(0xd43225, WorldObject.BIG_RED_MUSHROOM);
        worldObjects.put(0xbd591c, WorldObject.BIG_BROWN_MUSHROOM);
        worldObjects.put(0x4d6f4f, WorldObject.SWAMP_TREE);
                
        worldObjects.put(0xc90900, WorldObject.RED_ROSE);
        worldObjects.put(0xdbdc2a, WorldObject.YELLOW_FLOWER);
        worldObjects.put(0xd2c35e, WorldObject.DEAD_BUSH);
        worldObjects.put(0x7dc413, WorldObject.TALL_GRASS);
        worldObjects.put(0xc6c65c, WorldObject.DEAD_SHRUB);
        worldObjects.put(0x56ce08, WorldObject.FERN);
        worldObjects.put(0xc0746f, WorldObject.RED_MUSHROOM);
        worldObjects.put(0xc98358, WorldObject.BROWN_MUSHROOM);
        worldObjects.put(0x00500a, WorldObject.CACTUS);
        worldObjects.put(0xb4f374, WorldObject.SUGAR_CANE);

        worldObjects.put(0xababab, WorldObject.LIGHT_GRAY_WOOL);
        worldObjects.put(0x6a6a6a, WorldObject.GRAY_WOOL);
        worldObjects.put(0x000000, WorldObject.BLACK_WOOL);
        worldObjects.put(0xc30000, WorldObject.RED_WOOL);
        worldObjects.put(0xed9100, WorldObject.ORANGE_WOOL);
        worldObjects.put(0xeddf00, WorldObject.YELLOW_WOOL);
        worldObjects.put(0x83d41c, WorldObject.LIME_WOOL);
        worldObjects.put(0x3c521c, WorldObject.GREEN_WOOL);
        worldObjects.put(0x8fb9f4, WorldObject.LIGHT_BLUE_WOOL);
        worldObjects.put(0x1a7d84, WorldObject.CYAN_WOOL);
        worldObjects.put(0x224baf, WorldObject.BLUE_WOOL);
        worldObjects.put(0xa453ce, WorldObject.PURPLE_WOOL);
        worldObjects.put(0xd947d0, WorldObject.MAGENTA_WOOL);
        worldObjects.put(0xeda7cb, WorldObject.PINK_WOOL);
        worldObjects.put(0x6d4c35, WorldObject.BROWN_WOOL);
    }

    public static void loadJsonBuilder(GsonBuilder builder) {
        builder.registerTypeAdapter(WorldConfig.class, new WorldConfigSerializer());
        builder.registerTypeAdapter(Material.class, new MaterialSerializer());
        builder.registerTypeAdapter(Biome.class, new BiomeSerializer());
        BlockPattern.loadJsonBuilder(builder);
        WorldObject.loadJsonBuilder(builder);
    }

    private static class WorldConfigSerializer implements JsonSerializer<WorldConfig>, JsonDeserializer<WorldConfig> {
        @Override
        public WorldConfig deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
            WorldConfig config = new WorldConfig(false);
            JsonObject object = jsonElement.getAsJsonObject();
            JsonElement biomes = object.get("biomes");
            if(biomes != null) {
                for (Map.Entry<String, JsonElement> entry : biomes.getAsJsonObject().entrySet()) {
                    Biome b = jsonDeserializationContext.deserialize(entry.getValue(),Biome.class);
                    config.biomes.put(Integer.parseInt(entry.getKey(),16),b);
                }
            }

            JsonElement blocks = object.get("blocks");
            if(blocks != null) {
                for (Map.Entry<String, JsonElement> entry : blocks.getAsJsonObject().entrySet()) {
                    BlockPattern pattern = jsonDeserializationContext.deserialize(entry.getValue(), BlockPattern.class);
                    config.blockPatterns.put(Integer.parseInt(entry.getKey(),16),pattern);
                }
            }

            JsonElement objects = object.get("objects");
            if(objects != null) {
                for (Map.Entry<String, JsonElement> entry : objects.getAsJsonObject().entrySet()) {
                    WorldObject obj = jsonDeserializationContext.deserialize(entry.getValue(),WorldObject.class);
                    if(obj == null) {
                        WorldCreator.logger.warning("Found invalid World Object " + entry.getValue().toString());
                        continue;
                    }
                    config.worldObjects.put(Integer.parseInt(entry.getKey(),16), obj);
                }
            }
            JsonElement liquid = object.get("liquid");
            if(liquid != null) {
                Material mat = jsonDeserializationContext.deserialize(liquid, Material.class);
                if(mat != null) config.liquid = mat;
            }

            JsonElement biomeIce = object.get("biomeice");
            if(biomeIce != null)
                config.biomeIce = biomeIce.getAsBoolean();
            
            return config;
        }

        @Override
        public JsonElement serialize(WorldConfig worldConfig, Type type, JsonSerializationContext jsonSerializationContext) {
            JsonObject result = new JsonObject();

            result.add("version", new JsonPrimitive(worldConfig.version));
            result.add("liquid", jsonSerializationContext.serialize(worldConfig.liquid,Material.class));
            result.add("biomeice", new JsonPrimitive(worldConfig.biomeIce));
            JsonObject biomes = new JsonObject();
            for(Map.Entry<Integer,Biome> entry : worldConfig.biomes.entrySet()) {
                biomes.add(toHex(entry.getKey()), jsonSerializationContext.serialize(entry.getValue(),Biome.class));
            }
            result.add("biomes",biomes);
            
            JsonObject blocks = new JsonObject();
            for(Map.Entry<Integer, BlockPattern> entry : worldConfig.blockPatterns.entrySet()) {
                blocks.add(toHex(entry.getKey()), jsonSerializationContext.serialize(entry.getValue(),BlockPattern.class));
            }
            result.add("blocks",blocks);

            JsonObject worldObjects = new JsonObject();
            for(Map.Entry<Integer, WorldObject> entry : worldConfig.worldObjects.entrySet()) {
                worldObjects.add(toHex(entry.getKey()), jsonSerializationContext.serialize(entry.getValue(),WorldObject.class));
            }
            result.add("objects",worldObjects);

            return result;
        }

        private String toHex(int val) {
            return String.format("%06x", val);
        }
    }
}
