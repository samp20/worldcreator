/*
 * Copyright 2012 zombiechickentaco.com
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.zct.worldcreator.model.serializers;

import com.google.gson.*;
import org.bukkit.block.Biome;

import java.lang.reflect.Type;


public class BiomeSerializer implements JsonSerializer<Biome>, JsonDeserializer<Biome> {
    @Override
    public Biome deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        return Biome.valueOf(jsonElement.getAsString().toUpperCase().replaceAll("\\s+","_"));
    }

    @Override
    public JsonElement serialize(Biome biome, Type type, JsonSerializationContext jsonSerializationContext) {
        return new JsonPrimitive(biome.name().replace("_"," ").toLowerCase());
    }
}
