/*
 * Copyright 2012 zombiechickentaco.com
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.zct.worldcreator.model;

import com.google.gson.*;
import org.apache.commons.lang.Validate;
import org.bukkit.Material;

import java.lang.reflect.Type;


public class MaterialStack {
    public Material material;
    public int count;

    public MaterialStack() {
        this(Material.AIR,1);
    }

    public MaterialStack(Material material) {
        this(material,1);
    }

    public MaterialStack(Material material, int count) {
        Validate.isTrue(count>0,"Count must be greater than 0");
        Validate.notNull(material,"Material cannot be null");
        this.material = material;
        this.count = count;
    }

    public static void loadJsonBuilder(GsonBuilder builder) {
        builder.registerTypeAdapter(MaterialStack.class, new MaterialStackSerializer());
    }

    private static class MaterialStackSerializer implements JsonSerializer<MaterialStack>, JsonDeserializer<MaterialStack> {


        @Override
        public MaterialStack deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
            String val = jsonElement.getAsString();
            int count = 1;
            if(val.contains(":")) {
                int colon = val.indexOf(":");
                count = Integer.parseInt(val.substring(colon+1));
                val = val.substring(0,colon);
            }
            Material material = Material.matchMaterial(val);
            if(material == null) throw new JsonIOException("Unknown material " + val);
            return new MaterialStack(material,count);
        }

        @Override
        public JsonElement serialize(MaterialStack materialStack, Type type, JsonSerializationContext jsonSerializationContext) {
            String material = materialStack.material.name().replace('_',' ').toLowerCase();
            if(materialStack.count > 1) {
                return new JsonPrimitive(material + ":" + materialStack.count);
            }else{
                return new JsonPrimitive(material);
            }
        }
    }
}
