/*
 * Copyright 2012 zombiechickentaco.com
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.zct.worldcreator.model;

import com.google.gson.*;

import java.lang.reflect.Type;


public enum WorldObject {
    NONE,
    //Trees
    TREE,
    BIG_TREE,
    REDWOOD_TREE,
    TALL_REDWOOD_TREE,
    BIRCH_TREE,
    JUNGLE_TREE,
    SMALL_JUNGLE_TREE,
    JUNGLE_BUSH,
    BIG_RED_MUSHROOM,
    BIG_BROWN_MUSHROOM,
    SWAMP_TREE,

    //Misc
    RED_ROSE,
    YELLOW_FLOWER,
    DEAD_BUSH,
    TALL_GRASS,
    DEAD_SHRUB,
    FERN,
    RED_MUSHROOM,
    BROWN_MUSHROOM,
    CACTUS,
    SUGAR_CANE,

    //Wool
    LIGHT_GRAY_WOOL,
    GRAY_WOOL,
    BLACK_WOOL,
    RED_WOOL,
    ORANGE_WOOL,
    YELLOW_WOOL,
    LIME_WOOL,
    GREEN_WOOL,
    LIGHT_BLUE_WOOL,
    CYAN_WOOL,
    BLUE_WOOL,
    PURPLE_WOOL,
    MAGENTA_WOOL,
    PINK_WOOL,
    BROWN_WOOL;
    
    public static WorldObject matchObject(String name) {
        name = name.toUpperCase().replaceAll("\\s+","_");
        try{
            return WorldObject.valueOf(name);
        }catch (IllegalArgumentException ex) {
            return null;
        }
    }

    public static void loadJsonBuilder(GsonBuilder builder) {
        builder.registerTypeAdapter(WorldObject.class, new WorldObjectSerializer());
    }

    private static class WorldObjectSerializer implements JsonSerializer<WorldObject>, JsonDeserializer<WorldObject> {

        @Override
        public WorldObject deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
            return WorldObject.matchObject(jsonElement.getAsString());
        }

        @Override
        public JsonElement serialize(WorldObject worldObject, Type type, JsonSerializationContext jsonSerializationContext) {
            return new JsonPrimitive(worldObject.name().replace('_',' ').toLowerCase());
        }
    }
    

}
