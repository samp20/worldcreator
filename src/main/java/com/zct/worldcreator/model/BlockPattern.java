/*
 * Copyright 2012 zombiechickentaco.com
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.zct.worldcreator.model;

import com.google.gson.*;
import org.bukkit.Material;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class BlockPattern {
    private List<Integer> blocks;
    private int middle;
    
    public BlockPattern(int middle, Integer... blocks) {
        this(middle,Arrays.asList(blocks));
    }

    public BlockPattern(int middle, List<Integer> blocks) {
        if(blocks.size() < 0) throw new IllegalArgumentException("Must provide one or more blocks");
        if(middle < 0) middle = 0;
        if(middle >= blocks.size()) middle = blocks.size()-1;
        this.blocks = blocks;
        this.middle = middle;
    }

    public int getBlock(int min, int max, int pos) {
        pos-=min;
        max-=min;
        if(pos < middle) return blocks.get(pos);
        pos -= max -(blocks.size()- middle - 1);
        if(pos < 0) return blocks.get(middle);
        return blocks.get(pos+middle+1);
    }
    /*
    middle = 2
    (6)  0,1,[2],3,4,5
    (10) 0,1,2,3,4,5,6,7,8,9
     */

    public static void loadJsonBuilder(GsonBuilder builder) {
        MaterialStack.loadJsonBuilder(builder);
        builder.registerTypeAdapter(BlockPattern.class, new BlockPatternSerializer());
    }

    private static class BlockPatternSerializer implements JsonSerializer<BlockPattern>, JsonDeserializer<BlockPattern> {
        @Override
        public BlockPattern deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {

            JsonObject object = jsonElement.getAsJsonObject();
            int middle = object.get("middle").getAsInt();
            List<Integer> blocks = new ArrayList<Integer>();
            for(JsonElement block : object.get("blocks").getAsJsonArray()) {
                MaterialStack stack = jsonDeserializationContext.deserialize(block, MaterialStack.class);
                for(int i=0; i<stack.count; i++) {
                    blocks.add(stack.material.getId());
                }
            }
            return new BlockPattern(middle,blocks);
        }

        @Override
        public JsonElement serialize(BlockPattern blockPattern, Type type, JsonSerializationContext jsonSerializationContext) {
            JsonObject result = new JsonObject();
            result.addProperty("middle",blockPattern.middle);
            JsonArray array = new JsonArray();

            MaterialStack materialStack=null;
            for(int block : blockPattern.blocks) {
                Material material = Material.getMaterial(block);
                if(materialStack != null && material.equals(materialStack.material)) {
                    materialStack.count++;
                }else{
                    if(materialStack != null)
                        array.add(jsonSerializationContext.serialize(materialStack,MaterialStack.class));
                    materialStack = new MaterialStack(material);
                }
            }

            if(materialStack != null) array.add(jsonSerializationContext.serialize(materialStack,MaterialStack.class));

            result.add("blocks",array);
            return result;
        }
    }
}
