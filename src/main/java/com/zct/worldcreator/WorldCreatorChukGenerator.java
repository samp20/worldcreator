/*
 * Copyright 2012 zombiechickentaco.com
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package com.zct.worldcreator;

import com.zct.worldcreator.cache.TextureCache;
import com.zct.worldcreator.model.WorldConfig;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Biome;
import org.bukkit.generator.BlockPopulator;
import org.bukkit.generator.ChunkGenerator;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class WorldCreatorChukGenerator extends ChunkGenerator {
    
    @Override
    public byte[][] generateBlockSections(World world, Random random, int cx, int cz, BiomeGrid biomes) {
        byte[][] data = new byte[16][];

        int rx = cx<0? (cx-31)/32 : cx/32;
        int rz = cz<0? (cz-31)/32 : cz/32;

        WorldConfig config = WorldCreator.getInstance().getWorldCache().get(world);
        TextureCache cache = WorldCreator.getInstance().getTextureCache();

        BufferedImage blockMap = getSubImage(cache.get(world,rx,rz, TextureCache.TextureType.BLOCK_MAP), cx, cz);

        BufferedImage heightMap = getSubImage(cache.get(world, rx, rz, TextureCache.TextureType.HEIGHT_MAP), cx, cz);

        BufferedImage biomeMap = getSubImage(cache.get(world, rx, rz, TextureCache.TextureType.BIOME_MAP), cx, cz);

        if(heightMap == null) return data;

        for(int x=0; x< 16; x++) {
            for(int z=0; z< 16; z++) {
                Color c = new Color(heightMap.getRGB(x,z));
                int maxBlockHeight = c.getRed();
                int minBlockHeight = c.getGreen();
                int waterHeight = c.getBlue();
                
                int maxHeight = Math.max(maxBlockHeight,waterHeight);

                if(biomeMap != null) {
                    biomes.setBiome(x,z,config.getBiome(biomeMap.getRGB(x, z)));
                }else{
                    biomes.setBiome(x,z,config.getDefaultBiome());
                }


                int block = 0;
                if(blockMap != null) {
                    block = blockMap.getRGB(x,z);
                }
                boolean icyWater = false;
                if(config.biomeIce) {
                    Biome b = biomes.getBiome(x,z);
                    icyWater = b==Biome.FROZEN_RIVER ||
                            b==Biome.FROZEN_OCEAN ||
                            b==Biome.ICE_DESERT ||
                            b==Biome.ICE_MOUNTAINS ||
                            b==Biome.ICE_PLAINS ||
                            b==Biome.TAIGA ||
                            b==Biome.TAIGA_HILLS;
                }

                for(int yy=minBlockHeight/16; yy*16 <= maxHeight; yy++) {
                    if(data[yy] == null) data[yy] = new byte[4096];
                    for(int y= Math.max(0,minBlockHeight - yy*16); y+(yy*16) < maxHeight && y < 16; y++) {
                        int yt = y+yy*16;
                        byte blockType;
                        if(yt >= maxBlockHeight) {
                            if(icyWater && (yt+1)==maxHeight) {
                                blockType = (byte) Material.ICE.getId();
                            }else{
                                blockType = (byte)config.liquid.getId();
                            }
                        }else{
                            blockType = (byte)config.getBlockPattern(block).getBlock(minBlockHeight,maxBlockHeight,yt);
                        }
                        data[yy][(y * 16 + z) * 16 + x] = blockType;

                    }
                }
                if(icyWater && maxHeight <= 255 && maxHeight > 0) {
                    Material topBlock = Material.getMaterial(data[(maxHeight-1)/16][(((maxHeight-1)%16) * 16 + z) * 16 + x]);
                    if(topBlock.isBlock() &&
                            topBlock != Material.STATIONARY_WATER &&
                            topBlock != Material.STATIONARY_LAVA &&
                            topBlock != Material.ICE &&
                            topBlock != Material.WATER &&
                            topBlock != Material.LAVA) {
                        int max2 = (maxHeight)/16;
                        if(data[max2] == null) data[max2] = new byte[4096];
                        data[max2][(((maxHeight)%16) * 16 + z) * 16 + x] = (byte)Material.SNOW.getId();
                    }
                }
            }
        }
        return data;
    }

    @Override
    public boolean canSpawn(World world, int x, int z) {
        return super.canSpawn(world, x, z);    //To change body of overridden methods use File | Settings | File Templates.
    }

    @Override
    public List<BlockPopulator> getDefaultPopulators(World world) {
        List<BlockPopulator> list = new ArrayList<BlockPopulator>();
        list.add(new ObjectBlockPopulator());
        return list;
    }

    @Override
    public Location getFixedSpawnLocation(World world, Random random) {
        return world.getHighestBlockAt(0,0).getLocation();
    }

    private BufferedImage getSubImage(BufferedImage source, int cx, int cz) {
        if(source==null) return null;
        return source.getSubimage((cx&0x1f)*16,(cz&0x1f)*16,16,16);
    }
    
    
}
